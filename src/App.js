import React, {Component} from 'react';
import './App.css';
import Lottery from "./Lottery/Lottery";

class App extends Component {

    randNumber() {
        let data = [];
        for (let i = 1; i <= 5; i++) {
            const random_number = Math.floor(Math.random() * 32) + 5;
            data.push(random_number);
        }
        data.sort(function (a, b) {
            return a - b;
        });
        return data;
    }

    constructor(props) {
        super(props);
        this.state = {numbers: this.randNumber()}
    }

    changeNumber = () => {
        this.setState(
            {numbers: this.randNumber()},
        );
    };

    render() {
        return (
            <div className="App">
                <div>
                    <button className='button' onClick={this.changeNumber}>Change numbers</button>
                </div>
                <Lottery number={this.state.numbers[0]}/>
                <Lottery number={this.state.numbers[1]}/>
                <Lottery number={this.state.numbers[2]}/>
                <Lottery number={this.state.numbers[3]}/>
                <Lottery number={this.state.numbers[4]}/>
            </div>
        );
    }

}

export default App;
